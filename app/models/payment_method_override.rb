class PaymentMethodOverride < ActiveRecord::Base
  extend Enumerize

  attr_accessible :name, :description, :payment_method, :payment_frequency, :fixed_customer_payment, :brokerage_customer_percentage, :per_acp_customer_percentage, :per_acp_acp_schedule_id

  validates :name, :payment_method, :payment_frequency, presence: true
  validates :brokerage_customer_percentage, :per_acp_customer_percentage, :inclusion => 0..1, allow_blank: true

  enumerize :payment_method, in: [:fixed, :per_acp, :brokerage, :spot]
  enumerize :payment_frequency, in: [:annually, :semiannually, :quarterly, :never]

  belongs_to :per_acp_acp_schedule, class_name: 'AcpSchedule'
end
