class PriceBucketsController < ApplicationController
  add_breadcrumb I18n.t('dashboard'), :root_path
  add_breadcrumb I18n.t('clearing_price_buckets_model'), :price_buckets_path

  def index
    @price_buckets = ClearingPriceBucket.order('created_at DESC')
  end

  def new
    @price_bucket = ClearingPriceBucket.new

    add_breadcrumb I18n.t('clearing_price_buckets.new')
  end

  def show
    @price_bucket = ClearingPriceBucket.find(params[:id])

    add_breadcrumb @price_bucket.name
  end

  def edit
    @price_bucket = ClearingPriceBucket.find(params[:id])

    add_breadcrumb @price_bucket.name, price_bucket_path(@price_bucket)
    add_breadcrumb I18n.t('edit')
  end

  def create
    @price_bucket = ClearingPriceBucket.new(params[:clearing_price_bucket])

    if @price_bucket.save
      redirect_to price_buckets_path, notice: "New '#{@price_bucket.name}' bucket was successfully created."
    else
      render :new
    end
  end

  def update
    @price_bucket = ClearingPriceBucket.find(params[:id])

    if @price_bucket.update_attributes(params[:clearing_price_bucket])
      redirect_to price_bucket_path(@price_bucket), notice: "Clearing Price Bucket '#{@price_bucket.name}' was successfully updated."
    else
      render :edit
    end
  end

  def destroy
    @price_bucket = ClearingPriceBucket.find(params[:id]).destroy
    redirect_to price_buckets_path, notice: "Clearing Price Bucket '#{@price_bucket.name}' was successfully deleted."
  end

  def destroy_multiple
    if params[:clearing_price_buckets].present?
      ClearingPriceBucket.destroy(params[:clearing_price_buckets])
      redirect_to price_buckets_path, notice: "Clearing Price Buckets with id #{params[:clearing_price_buckets].join(',')} was successfully deleted."
    else
      redirect_to price_buckets_path, alert: 'No clearing price buckets was selected'
    end
  end
end
