require 'spec_helper'

describe 'User' do
  let(:user) { create :user }

  before(:each) { sign_in_with user }

  it { user_should_be_signed_in }

  describe "views list of existing payment_method_override records: " do

    context 'User navigates to Dashboard' do
      before { visit root_path }

      it { page.status_code.should eq 200 }
      it { page.body.should have_selector('li.active', text: I18n.t('dashboard')) }

      context "then user hovers over the 'Administration' link, clicks 'Payment Method Overrides' link" do
        before do
          click_link I18n.t('administration')
          click_link I18n.t('payment_method_overrides_model')
        end

        it { page.status_code.should eq 200 }
        it { current_path.should be_eql payment_method_overrides_path }

        it { page.body.should have_selector('table') }
        it { page.body.should have_selector('th', text: I18n.t('payment_method_overrides.id')) }
        it { page.body.should have_selector('th', text: I18n.t('payment_method_overrides.name')) }

        it { page.body.should have_selector('a', text: I18n.t('payment_method_overrides.create_new')) }
        it { find_button(I18n.t('payment_method_overrides.delete_selected')).should be }

      end
    end
  end

  describe "creates new payment_method_override record: " do
    context 'User navigates to Dashboard' do
      before { visit root_path }

      it { page.status_code.should eq 200 }
      it { page.body.should have_selector('li.active', text: I18n.t('dashboard')) }

      context "then user hovers over the 'Administration' link, clicks 'Payment Method Overrides' link" do
        before do
          click_link I18n.t('administration')
          click_link I18n.t('payment_method_overrides_model')
        end

        it { page.status_code.should eq 200 }
        it { current_path.should be_eql payment_method_overrides_path }

        context "User clicks the link in the left sidebar menu titled 'Create New payment method override'" do
          before { click_link I18n.t('payment_method_overrides.create_new') }

          it { page.body.should have_selector('form#new_payment_method_override') }

          context 'then cancels payment_method_override creation' do
            before do
              click_link(I18n.t('cancel'))
            end

            it { current_path.should be_eql payment_method_overrides_path }
            it { page.should_not have_selector('.alert') }
          end

          context 'fill form with valid data' do
            before do
              fill_in(I18n.t('payment_method_overrides.name'), :with => 'Method Override for test')
              select('Brokerage', :from => I18n.t('payment_method_overrides.payment_method'))
              select('Annually', :from => I18n.t('payment_method_overrides.payment_frequency'))
              click_button(I18n.t('save'))
            end

            it { current_path.should be_eql payment_method_overrides_path }
            it { page.should have_selector(".alert-success", text: "Method Override for test was successfully created.") }
          end

          context 'fill form with invalid data' do
            before do
              fill_in(I18n.t('payment_method_overrides.name'), :with => 'Only name')
              click_button(I18n.t('save'))
            end

            it { page.should have_selector(".alert-danger", text: "#{I18n.t('payment_method_overrides.form_error_message')}") }
            it { page.should have_selector(".alert-danger ul li") }

            context 'then correct the errors' do
              before do
                fill_in(I18n.t('payment_method_overrides.name'), :with => 'Payment Method Override2 for test')
                select('Brokerage', :from => I18n.t('payment_method_overrides.payment_method'))
                select('Quarterly', :from => I18n.t('payment_method_overrides.payment_frequency'))
                click_button(I18n.t('save'))
              end

              it { current_path.should be_eql payment_method_overrides_path }
              it { page.should have_selector(".alert-success", text: "Payment Method Override2 for test was successfully created.") }
            end
          end
        end
      end
    end
  end

  describe "views single payment_method_override record: " do
    context 'User navigates to Dashboard' do
      before { visit root_path }

      it { page.status_code.should eq 200 }
      it { page.body.should have_selector('li.active', text: I18n.t('dashboard')) }

      context "then user hovers over the 'Administration' link, clicks 'Payment Method Overrides' link" do
        let!(:payment_method_override) { create :payment_method_override }

        before do
          click_link I18n.t('administration')
          click_link I18n.t('payment_method_overrides_model')
        end

        it { page.status_code.should eq 200 }
        it { current_path.should be_eql payment_method_overrides_path }

        context "then clicks link labeled 'View' next to a single existing payment_method_override record" do
          before { click_link I18n.t('view') }

          it { page.status_code.should eq 200 }
          it { current_path.should be_eql payment_method_override_path(payment_method_override) }

          it { page.body.should have_content(I18n.t('payment_method_overrides.name')) }

          it { page.body.should have_selector('a', text: I18n.t('payment_method_overrides.edit')) }
          it { page.body.should have_selector('a', text: I18n.t('payment_method_overrides.delete')) }

          context "after that, user clicks link labeled 'Edit payment_method_override', is taken to 'Edit payment_method_override' page" do
            before { click_link I18n.t('payment_method_overrides.edit') }

            it { page.status_code.should eq 200 }
            it { current_path.should be_eql edit_payment_method_override_path(payment_method_override) }

            context 'and fill the form with valid data' do
              before do
                fill_in(I18n.t('payment_method_overrides.name'), :with => 'Changed Payment Method Override for test')
                select('Fixed', :from => I18n.t('payment_method_overrides.payment_method'))
                select('Semi-Annually', :from => I18n.t('payment_method_overrides.payment_frequency'))
                click_button(I18n.t('save'))
              end

              it { current_path.should be_eql payment_method_override_path(payment_method_override) }
              it { page.should have_selector(".alert-success", text: "Changed Payment Method Override for test was successfully updated.") }
              it { page.should have_selector("td", text: "Changed Payment Method Override") }
              it { page.should have_selector("td", text: "Fixed") }
              it { page.should have_selector("td", text: "Semi-Annually") }
            end

            context 'and fill the form with invalid data' do
              before do
                fill_in(I18n.t('payment_method_overrides.name'), :with => '')
                click_button(I18n.t('save'))
              end

              it { current_path.should be_eql payment_method_override_path(payment_method_override) }
              it { page.should have_selector(".alert-danger", text: "#{I18n.t('payment_method_overrides.form_error_message')}") }
              it { page.should have_selector(".alert-danger ul li") }

              context 'then correct the mistake' do
                before do
                  fill_in(I18n.t('payment_method_overrides.name'), :with => 'Payment Method Override name')
                  click_button(I18n.t('save'))
                end

                it { current_path.should be_eql payment_method_override_path(payment_method_override) }
                it { page.should have_selector(".alert-success", text: "Payment Method Override name was successfully updated.") }
                it { page.should have_selector("td", text: "Payment Method Override name") }
              end

              context 'then cancels payment_method_override creation' do
                before do
                  click_link(I18n.t('cancel'))
                end

                it { current_path.should be_eql payment_method_overrides_path }
                it { page.should_not have_selector('.alert') }
              end
            end
          end
        end
      end
    end
  end

  describe "deletes a single payment_method_override record when viewing a single payment_method_override record: " do
    context 'User navigates to Dashboard' do
      before { visit root_path }

      it { page.status_code.should eq 200 }
      it { page.body.should have_selector('li.active', text: I18n.t('dashboard')) }

      context "then user hovers over the 'Administration' link, clicks 'Payment Method Overrides' link" do
        let!(:another_clearing_price_bucket) { create :payment_method_override, name: 'Payment Method Override for delete' }

        before do
          click_link I18n.t('administration')
          click_link I18n.t('payment_method_overrides_model')
        end

        context "then clicks link labeled 'View' next to a single existing payment_method_override record" do
          before { first(:link, I18n.t('view')).click }

          it { page.status_code.should eq 200 }
          it { current_path.should be_eql payment_method_override_path(another_clearing_price_bucket) }

          it { page.body.should have_selector('a', text: I18n.t('payment_method_overrides.edit')) }
          it { page.body.should have_selector('a', text: I18n.t('payment_method_overrides.delete')) }
          it { page.should have_selector('td', text: 'Payment Method Override for delete') }

          context "after that, user clicks link labeled 'Delete payment_method_override', is taken to 'Edit payment_method_override' page", js: true do
            before do
              click_link I18n.t('payment_method_overrides.delete')
            end

            it { current_path.should be_eql payment_method_overrides_path }
            it { page.should_not have_selector('td', text: 'Payment Method Override for delete') }
            it { page.should have_selector(".alert-success", text: "Payment Method Override for delete was successfully deleted.") }
          end
        end
      end
    end
  end


  describe "deletes multiple payment_method_override records from the list of existing payment_method_override records: " do
    context 'User navigates to Dashboard' do
      before { visit root_path }

      it { page.status_code.should eq 200 }
      it { page.body.should have_selector('li.active', text: I18n.t('dashboard')) }

      context "then user hovers over the 'Administration' link, clicks 'Payment Method Overrides' link" do
        let!(:payment_method_override_one) { create :payment_method_override, name: 'payment_method_override one' }
        let!(:payment_method_override_two) { create :payment_method_override, name: 'payment_method_override two' }

        before do
          click_link I18n.t('administration')
          click_link I18n.t('payment_method_overrides_model')
        end

        it { page.should have_selector('td', text: 'payment_method_override one') }
        it { page.should have_selector('td', text: 'payment_method_override two') }

        context "then checks check box next to one or more records that they would like to delete", js: true do
          before do
            find(:css, "#payment_method_overrides_[value='#{payment_method_override_one.id}']").set(true)
            find(:css, "#payment_method_overrides_[value='#{payment_method_override_two.id}']").set(true)
            click_button(I18n.t('payment_method_overrides.delete_selected'))
          end

          it { current_path.should be_eql payment_method_overrides_path }
          it { page.should_not have_selector('td', text: 'payment_method_override one') }
          it { page.should_not have_selector('td', text: 'payment_method_override two') }
          it { page.should have_selector(".alert-success", text: 'Payment method overrides with id 1,2 was successfully deleted.') }
        end
      end
    end
  end
end
