class State < ActiveRecord::Base
  attr_accessible :name, :abbreviation, :is_system_state, :is_sale_state

  validates :name, :abbreviation, presence: true
  validates :is_system_state, :is_sale_state, inclusion: { in: [true, false] }

  has_many :acp_schedules
  has_many :clearing_price_buckets, foreign_key: :applicable_state_id
end
