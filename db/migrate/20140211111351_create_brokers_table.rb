class CreateBrokersTable < ActiveRecord::Migration
  def change
    create_table :brokers do |t|
      t.string :company_name
      t.string :contact_name
      t.string :contact_email
      t.string :contact_phone
      t.text :contact_address

      t.timestamps
    end
  end
end
