class CounterpartiesController < ApplicationController
  add_breadcrumb I18n.t('dashboard'), :root_path
  add_breadcrumb I18n.t('counterparties_model'), :counterparties_path

  def index
    @counterparties = Counterparty.order('created_at DESC')
  end

  def new
    @counterparty = Counterparty.new

    add_breadcrumb I18n.t('counterparties.new')
  end

  def show
    @counterparty = Counterparty.find(params[:id])

    add_breadcrumb @counterparty.company_name
  end

  def edit
    @counterparty = Counterparty.find(params[:id])

    add_breadcrumb @counterparty.company_name, counterparty_path(@counterparty)
    add_breadcrumb I18n.t('edit')
  end

  def create
    @counterparty = Counterparty.new(params[:counterparty])

    if @counterparty.save
      redirect_to counterparties_path, notice: "#{@counterparty.company_name} was successfully created."
    else
      render :new
    end
  end

  def update
    @counterparty = Counterparty.find(params[:id])

    if @counterparty.update_attributes(params[:counterparty])
      redirect_to counterparty_path(@counterparty), notice: "#{@counterparty.company_name} was successfully updated."
    else
      render :edit
    end
  end

  def destroy
    @counterparty = Counterparty.find(params[:id]).destroy
    redirect_to counterparties_path, notice: "#{@counterparty.company_name} was successfully deleted."
  end

  def destroy_multiple
    if params[:counterparties].present?
      Counterparty.destroy(params[:counterparties])
      redirect_to counterparties_path, notice: "Counterparties with id #{params[:counterparties].join(',')} was successfully deleted."
    else
      redirect_to counterparties_path, alert: 'No counterparties was selected'
    end
  end
end
