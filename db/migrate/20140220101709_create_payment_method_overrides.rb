class CreatePaymentMethodOverrides < ActiveRecord::Migration
  def change
    create_table :payment_method_overrides do |t|
      t.string :name
      t.text :description
      t.string :payment_method
      t.string :payment_frequency
      t.decimal :fixed_customer_payment, :precision => 9, :scale => 2
      t.decimal :brokerage_customer_percentage, :precision => 4, :scale => 3
      t.decimal :per_acp_customer_percentage, :precision => 4, :scale => 3
      t.integer :per_acp_acp_schedule_id

      t.timestamps
    end
  end
end
