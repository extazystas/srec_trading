require 'spec_helper'

describe "User logs in to the system through modal window:" do
  context 'User navigates to Dashboard' do
    let!(:user) { create :user }
    before { visit root_path }

    it { user_should_be_signed_out }

    context 'then clicks Login link', js: true do
      before do
        click_link I18n.t('login')
        fill_in('user_email', :with => user.email)
        fill_in('user_password', :with => user.password)
        click_button I18n.t('sign_in')
      end

      it { current_path.should be_eql root_path }
      it { page.should have_selector('.alert-success', text: 'Signed in successfully.') }
      it { page.should_not have_selector('.nav li a', text: I18n.t('login')) }
    end
  end
end
