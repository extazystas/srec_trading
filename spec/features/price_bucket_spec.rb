require 'spec_helper'

describe 'User' do
  let(:user) { create :user }

  before(:each) { sign_in_with user }

  it { user_should_be_signed_in }

  describe "views list of existing clearing_price_bucket records: " do

    context 'User navigates to Dashboard' do
      before { visit root_path }

      it { page.status_code.should eq 200 }
      it { page.body.should have_selector('li.active', text: I18n.t('dashboard')) }

      context "then user hovers over the 'Administration' link, clicks 'Clearing Price Buckets' link" do
        before do
          click_link I18n.t('administration')
          click_link I18n.t('clearing_price_buckets_model')
        end

        it { page.status_code.should eq 200 }
        it { current_path.should be_eql price_buckets_path }

        it { page.body.should have_selector('table') }
        it { page.body.should have_selector('th', text: I18n.t('clearing_price_buckets.id')) }
        it { page.body.should have_selector('th', text: I18n.t('clearing_price_buckets.name')) }

        it { page.body.should have_selector('a', text: I18n.t('clearing_price_buckets.create_new')) }
        it { find_button(I18n.t('clearing_price_buckets.delete_selected')).should be }

      end
    end
  end

  describe "creates new clearing_price_bucket record: " do
    context 'User navigates to Dashboard' do
      before { visit root_path }

      it { page.status_code.should eq 200 }
      it { page.body.should have_selector('li.active', text: I18n.t('dashboard')) }

      context "then user hovers over the 'Administration' link, clicks 'Clearing Price Buckets' link" do
        before do
          click_link I18n.t('administration')
          click_link I18n.t('clearing_price_buckets_model')
        end

        it { page.status_code.should eq 200 }
        it { current_path.should be_eql price_buckets_path }

        context "User clicks the link in the left sidebar menu titled 'Create New bucket'" do
          before { click_link I18n.t('clearing_price_buckets.create_new') }

          it { page.body.should have_selector('form#new_clearing_price_bucket') }

          context 'then cancels clearing_price_bucket creation' do
            before do
              click_link(I18n.t('cancel'))
            end

            it { current_path.should be_eql price_buckets_path }
            it { page.should_not have_selector('.alert') }
          end

          context 'fill form with valid data' do
            before do
              fill_in(I18n.t('clearing_price_buckets.name'), :with => 'Bucket for test')
              click_button(I18n.t('save'))
            end

            it { current_path.should be_eql price_buckets_path }
            it { page.should have_selector(".alert-success", text: "New 'Bucket for test' bucket was successfully created.") }
          end

          context 'fill form with invalid data' do
            before do
              click_button(I18n.t('save'))
            end

            it { page.should have_selector(".alert-danger", text: "#{I18n.t('clearing_price_buckets.form_error_message')}") }
            it { page.should have_selector(".alert-danger ul li") }

            context 'then correct the errors' do
              before do
                fill_in(I18n.t('clearing_price_buckets.name'), :with => 'Bucket2 for test')
                click_button(I18n.t('save'))
              end

              it { current_path.should be_eql price_buckets_path }
              it { page.should have_selector(".alert-success", text: "New 'Bucket2 for test' bucket was successfully created.") }
            end
          end
        end
      end
    end
  end

  describe "views single clearing_price_bucket record: " do
    context 'User navigates to Dashboard' do
      before { visit root_path }

      it { page.status_code.should eq 200 }
      it { page.body.should have_selector('li.active', text: I18n.t('dashboard')) }

      context "then user hovers over the 'Administration' link, clicks 'Clearing Price Buckets' link" do
        let!(:clearing_price_bucket) { create :clearing_price_bucket }

        before do
          click_link I18n.t('administration')
          click_link I18n.t('clearing_price_buckets_model')
        end

        it { page.status_code.should eq 200 }
        it { current_path.should be_eql price_buckets_path }

        context "then clicks link labeled 'View' next to a single existing clearing_price_bucket record" do
          before { click_link I18n.t('view') }

          it { page.status_code.should eq 200 }
          it { current_path.should be_eql price_bucket_path(clearing_price_bucket) }

          it { page.body.should have_content(I18n.t('clearing_price_buckets.name')) }

          it { page.body.should have_selector('a', text: I18n.t('clearing_price_buckets.edit')) }
          it { page.body.should have_selector('a', text: I18n.t('clearing_price_buckets.delete')) }

          context "after that, user clicks link labeled 'Edit bucket', is taken to 'Edit bucket' page" do
            before { click_link I18n.t('clearing_price_buckets.edit') }

            it { page.status_code.should eq 200 }
            it { current_path.should be_eql edit_price_bucket_path(clearing_price_bucket) }

            context 'and fill the form with valid data' do
              before do
                fill_in(I18n.t('clearing_price_buckets.name'), :with => 'Changed Bucket for test')
                click_button(I18n.t('save'))
              end

              it { current_path.should be_eql price_bucket_path(clearing_price_bucket) }
              it { page.should have_selector(".alert-success", text: "Clearing Price Bucket 'Changed Bucket for test' was successfully updated.") }
              it { page.should have_selector("td", text: "Changed Bucket") }
            end

            context 'and fill the form with invalid data' do
              before do
                fill_in(I18n.t('clearing_price_buckets.name'), :with => '')
                click_button(I18n.t('save'))
              end

              it { current_path.should be_eql price_bucket_path(clearing_price_bucket) }
              it { page.should have_selector(".alert-danger", text: "#{I18n.t('clearing_price_buckets.form_error_message')}") }
              it { page.should have_selector(".alert-danger ul li") }

              context 'then correct the mistake' do
                before do
                  fill_in(I18n.t('clearing_price_buckets.name'), :with => 'Bucket name')
                  click_button(I18n.t('save'))
                end

                it { current_path.should be_eql price_bucket_path(clearing_price_bucket) }
                it { page.should have_selector(".alert-success", text: "Clearing Price Bucket 'Bucket name' was successfully updated.") }
                it { page.should have_selector("td", text: "Bucket name") }
              end

              context 'then cancels clearing_price_bucket creation' do
                before do
                  click_link(I18n.t('cancel'))
                end

                it { current_path.should be_eql price_buckets_path }
                it { page.should_not have_selector('.alert') }
              end
            end
          end
        end
      end
    end
  end

  describe "deletes a single clearing_price_bucket record when viewing a single clearing_price_bucket record: " do
    context 'User navigates to Dashboard' do
      before { visit root_path }

      it { page.status_code.should eq 200 }
      it { page.body.should have_selector('li.active', text: I18n.t('dashboard')) }

      context "then user hovers over the 'Administration' link, clicks 'Clearing Price Buckets' link" do
        let!(:another_clearing_price_bucket) { create :clearing_price_bucket, name: 'Bucket for delete' }

        before do
          click_link I18n.t('administration')
          click_link I18n.t('clearing_price_buckets_model')
        end

        context "then clicks link labeled 'View' next to a single existing clearing_price_bucket record" do
          before { first(:link, I18n.t('view')).click }

          it { page.status_code.should eq 200 }
          it { current_path.should be_eql price_bucket_path(another_clearing_price_bucket) }

          it { page.body.should have_selector('a', text: I18n.t('clearing_price_buckets.edit')) }
          it { page.body.should have_selector('a', text: I18n.t('clearing_price_buckets.delete')) }
          it { page.should have_selector('td', text: 'Bucket for delete') }

          context "after that, user clicks link labeled 'Delete bucket', is taken to 'Edit bucket' page", js: true do
            before do
              click_link I18n.t('clearing_price_buckets.delete')
            end

            it { current_path.should be_eql price_buckets_path }
            it { page.should_not have_selector('td', text: 'Bucket for delete') }
            it { page.should have_selector(".alert-success", text: "Clearing Price Bucket 'Bucket for delete' was successfully deleted.") }
          end
        end
      end
    end
  end


  describe "deletes multiple clearing_price_bucket records from the list of existing clearing_price_bucket records: " do
    context 'User navigates to Dashboard' do
      before { visit root_path }

      it { page.status_code.should eq 200 }
      it { page.body.should have_selector('li.active', text: I18n.t('dashboard')) }

      context "then user hovers over the 'Administration' link, clicks 'Clearing Price Buckets' link" do
        let!(:clearing_price_bucket_one) { create :clearing_price_bucket, name: 'bucket one' }
        let!(:clearing_price_bucket_two) { create :clearing_price_bucket, name: 'bucket two' }

        before do
          click_link I18n.t('administration')
          click_link I18n.t('clearing_price_buckets_model')
        end

        it { page.should have_selector('td', text: 'bucket one') }
        it { page.should have_selector('td', text: 'bucket two') }

        context "then checks check box next to one or more records that they would like to delete", js: true do
          before do
            find(:css, "#clearing_price_buckets_[value='#{clearing_price_bucket_one.id}']").set(true)
            find(:css, "#clearing_price_buckets_[value='#{clearing_price_bucket_two.id}']").set(true)
            click_button(I18n.t('clearing_price_buckets.delete_selected'))
          end

          it { current_path.should be_eql price_buckets_path }
          it { page.should_not have_selector('td', text: 'bucket one') }
          it { page.should_not have_selector('td', text: 'bucket two') }
          it { page.should have_selector(".alert-success", text: 'Clearing Price Buckets with id 1,2 was successfully deleted.') }
        end
      end
    end
  end
end
