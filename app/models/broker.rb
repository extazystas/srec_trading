class Broker < ActiveRecord::Base
  attr_accessible :company_name, :contact_name, :contact_email, :contact_address, :contact_phone

  validates :company_name, :contact_name, :contact_email, presence: true
  validates :contact_email, uniqueness: true
  validates :contact_email, format: { with: /\A([a-z0-9\-\_\.\+]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i }
  validates :contact_phone, format: { with: /\A(\(\d{3}\))\s(\d{3}\-\d{4})\z/i }, allow_blank: true
end
