class CreateCounterparties < ActiveRecord::Migration
  def change
    create_table :counterparties do |t|
      t.string :company_name
      t.string :contact_name
      t.string :contact_email
      t.string :contact_phone
      t.text :contact_address
      t.integer :gats_buyer_id

      t.timestamps
    end
  end
end
