require 'spec_helper'

describe BrokersController do
  login_user

  let!(:broker) { create :broker }

  describe '#index' do
    before { get :index }

    it { should respond_with(:success) }
    it { should render_template(:index) }
    it { should_not set_the_flash }
  end

  describe '#show' do
    before { get :show, { id: broker.id } }

    it { should respond_with(:success) }
    it { should render_template(:show) }
    it { should_not set_the_flash }
  end

  describe '#new' do
    before { get :new }

    it { should respond_with(:success) }
    it { should render_template(:new) }
    it { should_not set_the_flash }
  end

  describe '#edit' do
    before { get :edit, { id: broker.id } }

    it { should respond_with(:success) }
    it { should render_template(:edit) }
    it { should_not set_the_flash }
  end

  describe '#create' do
    let(:params) { { broker: { company_name: 'Company for test', contact_name: 'John Doe', contact_email: 'valid@email.com' } } }

    describe "with valid params" do
      before { post :create, params }

      it { should respond_with(:redirect) }
      it { should set_the_flash }
    end

    describe "with invalid params" do
      before do
        params[:broker][:company_name] = nil
        post :create, params
      end

      it { should render_template(:new) }
      it { should_not set_the_flash }
    end
  end

  describe '#update' do
    let(:params) { { broker: { company_name: 'Company for test', contact_name: 'John Doe', contact_email: 'valid@email.com' }, id: broker.id } }

    describe "with valid params" do
      before { post :update, params }

      it { should respond_with(:redirect) }
      it { should set_the_flash }
    end

    describe "with invalid params" do
      before do
        params[:broker][:contact_name] = nil
        post :create, params
      end

      it { should render_template(:new) }
      it { should_not set_the_flash }
    end
  end

  describe '#destroy' do
    before do
      @broker_count = Broker.count
      post :destroy, { id: broker.id }
    end

    it { Broker.count.should be_eql(@broker_count - 1) }
    it { should redirect_to(brokers_path) }
    it { should set_the_flash[:notice] }
  end

  describe '#destroy_multiple' do
    context 'with selected brokers' do
      let!(:broker_one) { create :broker }
      let!(:broker_two) { create :broker }

      before do
        @broker_count = Broker.count
        post :destroy_multiple, { brokers: [broker_one.id, broker_two.id] }
      end

      it { Broker.count.should be_eql(@broker_count - 2) }
      it { should redirect_to(brokers_path) }
      it { should set_the_flash[:notice] }
    end

     context 'without selected brokers' do
      before do
        @broker_count = Broker.count
        post :destroy_multiple
      end

      it { Broker.count.should be_eql(@broker_count) }
      it { should redirect_to(brokers_path) }
      it { should set_the_flash[:alert] }
    end
  end
end
