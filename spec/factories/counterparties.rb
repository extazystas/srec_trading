FactoryGirl.define do
  factory :counterparty do
    sequence(:company_name) { |n| "Company ##{n}" }
    sequence(:contact_name) { |n| "Broker ##{n}" }
    sequence(:contact_email) { |n| "fake#{n}@email.com" }
    contact_address 'New York, Test str, 123'
    contact_phone '(333) 678-0987'
  end
end
