FactoryGirl.define do
  factory :payment_method_override do
    name 'Payment method override name'
    description 'Payment method override description'
    payment_method 'fixed'
    payment_frequency 'annually'
  end
end
