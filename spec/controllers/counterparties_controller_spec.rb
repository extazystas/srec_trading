require 'spec_helper'

describe CounterpartiesController do
  login_user

  let!(:counterparty) { create :counterparty }

  describe '#index' do
    before { get :index }

    it { should respond_with(:success) }
    it { should render_template(:index) }
    it { should_not set_the_flash }
  end

  describe '#show' do
    before { get :show, { id: counterparty.id } }

    it { should respond_with(:success) }
    it { should render_template(:show) }
    it { should_not set_the_flash }
  end

  describe '#new' do
    before { get :new }

    it { should respond_with(:success) }
    it { should render_template(:new) }
    it { should_not set_the_flash }
  end

  describe '#edit' do
    before { get :edit, { id: counterparty.id } }

    it { should respond_with(:success) }
    it { should render_template(:edit) }
    it { should_not set_the_flash }
  end

  describe '#create' do
    let(:params) { { counterparty: { company_name: 'Company for test', contact_name: 'John Doe', contact_email: 'valid@email.com' } } }

    describe "with valid params" do
      before { post :create, params }

      it { should respond_with(:redirect) }
      it { should set_the_flash }
    end

    describe "with invalid params" do
      before do
        params[:counterparty][:company_name] = nil
        post :create, params
      end

      it { should render_template(:new) }
      it { should_not set_the_flash }
    end
  end

  describe '#update' do
    let(:params) { { counterparty: { company_name: 'Company for test', contact_name: 'John Doe', contact_email: 'valid@email.com' }, id: counterparty.id } }

    describe "with valid params" do
      before { post :update, params }

      it { should respond_with(:redirect) }
      it { should set_the_flash }
    end

    describe "with invalid params" do
      before do
        params[:counterparty][:contact_name] = nil
        post :create, params
      end

      it { should render_template(:new) }
      it { should_not set_the_flash }
    end
  end

  describe '#destroy' do
    before do
      @counterparties_count = Counterparty.count
      post :destroy, { id: counterparty.id }
    end

    it { Counterparty.count.should be_eql(@counterparties_count - 1) }
    it { should redirect_to(counterparties_path) }
    it { should set_the_flash[:notice] }
  end

  describe '#destroy_multiple' do
    context 'with selected counterparties' do
      let!(:counterparty_one) { create :counterparty }
      let!(:counterparty_two) { create :counterparty }

      before do
        @counterparties_count = Counterparty.count
        post :destroy_multiple, { counterparties: [counterparty_one.id, counterparty_two.id] }
      end

      it { Counterparty.count.should be_eql(@counterparties_count - 2) }
      it { should redirect_to(counterparties_path) }
      it { should set_the_flash[:notice] }
    end

     context 'without selected counterparties' do
      before do
        @counterparties_count = Counterparty.count
        post :destroy_multiple
      end

      it { Counterparty.count.should be_eql(@counterparties_count) }
      it { should redirect_to(counterparties_path) }
      it { should set_the_flash[:alert] }
    end
  end
end
