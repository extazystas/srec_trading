require 'spec_helper'

describe PriceBucketsController do
  login_user

  let!(:clearing_price_bucket) { create :clearing_price_bucket }

  describe '#index' do
    before { get :index }

    it { should respond_with(:success) }
    it { should render_template(:index) }
    it { should_not set_the_flash }
  end

  describe '#show' do
    before { get :show, { id: clearing_price_bucket.id } }

    it { should respond_with(:success) }
    it { should render_template(:show) }
    it { should_not set_the_flash }
  end

  describe '#new' do
    before { get :new }

    it { should respond_with(:success) }
    it { should render_template(:new) }
    it { should_not set_the_flash }
  end

  describe '#edit' do
    before { get :edit, { id: clearing_price_bucket.id } }

    it { should respond_with(:success) }
    it { should render_template(:edit) }
    it { should_not set_the_flash }
  end

  describe '#create' do
    let(:params) { { clearing_price_bucket: { name: 'Bucket name', description: 'Bucket description' } } }

    describe "with valid params" do
      before { post :create, params }

      it { should respond_with(:redirect) }
      it { should set_the_flash }
    end

    describe "with invalid params" do
      before do
        params[:clearing_price_bucket][:name] = nil
        post :create, params
      end

      it { should render_template(:new) }
      it { should_not set_the_flash }
    end
  end

  describe '#update' do
    let(:params) { { clearing_price_bucket: { name: 'Changed Bucket name', description: 'Bucket description' }, id: clearing_price_bucket.id } }

    describe "with valid params" do
      before { post :update, params }

      it { should respond_with(:redirect) }
      it { should set_the_flash }
    end

    describe "with invalid params" do
      before do
        params[:clearing_price_bucket][:name] = nil
        post :create, params
      end

      it { should render_template(:new) }
      it { should_not set_the_flash }
    end
  end

  describe '#destroy' do
    before do
      @clearing_price_buckets_count = ClearingPriceBucket.count
      post :destroy, { id: clearing_price_bucket.id }
    end

    it { ClearingPriceBucket.count.should be_eql(@clearing_price_buckets_count - 1) }
    it { should redirect_to(price_buckets_path) }
    it { should set_the_flash[:notice] }
  end

  describe '#destroy_multiple' do
    context 'with selected clearing_price_buckets' do
      let!(:clearing_price_bucket_one) { create :clearing_price_bucket }
      let!(:clearing_price_bucket_two) { create :clearing_price_bucket }

      before do
        @clearing_price_buckets_count = ClearingPriceBucket.count
        post :destroy_multiple, { clearing_price_buckets: [clearing_price_bucket_one.id, clearing_price_bucket_two.id] }
      end

      it { ClearingPriceBucket.count.should be_eql(@clearing_price_buckets_count - 2) }
      it { should redirect_to(price_buckets_path) }
      it { should set_the_flash[:notice] }
    end

     context 'without selected clearing_price_buckets' do
      before do
        @clearing_price_buckets_count = ClearingPriceBucket.count
        post :destroy_multiple
      end

      it { ClearingPriceBucket.count.should be_eql(@clearing_price_buckets_count) }
      it { should redirect_to(price_buckets_path) }
      it { should set_the_flash[:alert] }
    end
  end
end
