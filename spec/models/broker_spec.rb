require 'spec_helper'

describe Broker do
  let(:broker) { create :broker }

  it { should validate_presence_of(:company_name) }
  it { should validate_presence_of(:contact_name) }
  it { should validate_presence_of(:contact_email) }
  it { should validate_uniqueness_of(:contact_email) }

  context :contact_email do
    it { expect(broker).to allow_value('some_address@example.com').for(:contact_email) }
    it { expect(broker).to allow_value('broker.name@example.com').for(:contact_email) }
    it { expect(broker).to allow_value('some-address@example.com').for(:contact_email) }
    it { expect(broker).to_not allow_value('hey@address@example.com').for(:contact_email) }
    it { expect(broker).to_not allow_value('wrong%name@example.com').for(:contact_email) }
    it { expect(broker).to_not allow_value('wrong%name@example.c').for(:contact_email) }
    it { expect(broker).to_not allow_value('wrong!name@example').for(:contact_email) }
  end

  context :contact_phone do
    it { expect(broker).to allow_value('(555) 123-0987').for(:contact_phone) }
    it { expect(broker).to allow_value('(555) 333-0222').for(:contact_phone) }
    it { expect(broker).to_not allow_value('555 333 0222').for(:contact_phone) }
    it { expect(broker).to_not allow_value('(555) 333 0222').for(:contact_phone) }
    it { expect(broker).to_not allow_value('4444555555').for(:contact_phone) }
    it { expect(broker).to_not allow_value('12 554 871').for(:contact_phone) }
  end
end
