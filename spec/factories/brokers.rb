FactoryGirl.define do
  factory :broker do
    sequence(:company_name) { |n| "Company ##{n}" }
    sequence(:contact_name) { |n| "Broker ##{n}" }
    sequence(:contact_email) { |n| "fake#{n}@email.com" }
    contact_address 'London, Baker str, 221B'
    contact_phone '(555) 678-0987'
  end
end
