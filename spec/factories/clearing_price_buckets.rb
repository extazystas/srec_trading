FactoryGirl.define do
  factory :clearing_price_bucket do
    name 'Price Bucket name'
    description 'Price Bucket description'
  end
end
