require 'spec_helper'

describe ClearingPriceBucket do
  let(:clearing_price_bucket) { create :clearing_price_bucket }

  it { should validate_presence_of(:name) }

  it { should belong_to(:applicable_state) }
end
