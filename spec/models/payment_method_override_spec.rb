require 'spec_helper'

describe PaymentMethodOverride do
  let(:payment_method_override) { create :payment_method_override }

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:payment_method) }
  it { should validate_presence_of(:payment_frequency) }
  it {should ensure_inclusion_of(:brokerage_customer_percentage).in_range(0..1) }
  it {should ensure_inclusion_of(:per_acp_customer_percentage).in_range(0..1) }
  it { should allow_value("", nil).for(:brokerage_customer_percentage) }
  it { should allow_value("", nil).for(:per_acp_customer_percentage) }

  it { should belong_to(:per_acp_acp_schedule) }
end
