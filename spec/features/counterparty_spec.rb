require 'spec_helper'

describe 'User' do
  let(:user) { create :user }

  before(:each) { sign_in_with user }

  it { user_should_be_signed_in }

  describe "views list of existing counterparty records: " do

    context 'User navigates to Dashboard' do
      before { visit root_path }

      it { page.status_code.should eq 200 }
      it { page.body.should have_selector('li.active', text: I18n.t('dashboard')) }

      context "then user hovers over the 'Administration' link, clicks 'Counterparties' link" do
        before do
          click_link I18n.t('administration')
          click_link I18n.t('counterparties_model')
        end

        it { page.status_code.should eq 200 }
        it { current_path.should be_eql counterparties_path }

        it { page.body.should have_selector('table') }
        it { page.body.should have_selector('th', text: I18n.t('counterparties.id')) }
        it { page.body.should have_selector('th', text: I18n.t('counterparties.company_name')) }
        it { page.body.should have_selector('th', text: I18n.t('counterparties.contact_name')) }
        it { page.body.should have_selector('th', text: I18n.t('counterparties.contact_email')) }

        it { page.body.should have_selector('a', text: I18n.t('counterparties.create_new')) }
        it { find_button(I18n.t('counterparties.delete_selected')).should be }

      end
    end
  end

  describe "creates new counterparty record: " do
    context 'User navigates to Dashboard' do
      before { visit root_path }

      it { page.status_code.should eq 200 }
      it { page.body.should have_selector('li.active', text: I18n.t('dashboard')) }

      context "then user hovers over the 'Administration' link, clicks 'Counterparties' link" do
        before do
          click_link I18n.t('administration')
          click_link I18n.t('counterparties_model')
        end

        it { page.status_code.should eq 200 }
        it { current_path.should be_eql counterparties_path }

        context "User clicks the link in the left sidebar menu titled 'Create New Counterparty'" do
          before { click_link I18n.t('counterparties.create_new') }

          it { page.body.should have_selector('form#new_counterparty') }

          context 'then cancels counterparty creation' do
            before do
              click_link(I18n.t('cancel'))
            end

            it { current_path.should be_eql counterparties_path }
            it { page.should_not have_selector('.alert') }
          end

          context 'fill form with valid data' do
            before do
              fill_in(I18n.t('counterparties.company_name'), :with => 'Company for test')
              fill_in(I18n.t('counterparties.contact_name'), :with => 'John Doe')
              fill_in(I18n.t('counterparties.contact_email'), :with => 'valid@email.xyz')
              fill_in(I18n.t('counterparties.contact_phone'), :with => '(555) 123-0987')
              click_button(I18n.t('save'))
            end

            it { current_path.should be_eql counterparties_path }
            it { page.should have_selector(".alert-success", text: "Company for test was successfully created.") }
          end

          context 'fill form with invalid data' do
            before do
              click_button(I18n.t('save'))
            end

            it { page.should have_selector(".alert-danger", text: "#{I18n.t('counterparties.form_error_message')}") }
            it { page.should have_selector(".alert-danger ul li") }

            context 'then correct the errors' do
              before do
                fill_in(I18n.t('counterparties.company_name'), :with => 'Company2 for test')
                fill_in(I18n.t('counterparties.contact_name'), :with => 'John Doe')
                fill_in(I18n.t('counterparties.contact_email'), :with => 'valid@email.xyz')
                click_button(I18n.t('save'))
              end

              it { current_path.should be_eql counterparties_path }
              it { page.should have_selector(".alert-success", text: "Company2 for test was successfully created.") }
            end
          end
        end
      end
    end
  end

  describe "views single counterparty record: " do
    context 'User navigates to Dashboard' do
      before { visit root_path }

      it { page.status_code.should eq 200 }
      it { page.body.should have_selector('li.active', text: I18n.t('dashboard')) }

      context "then user hovers over the 'Administration' link, clicks 'Counterparties' link" do
        let!(:counterparty) { create :counterparty }

        before do
          click_link I18n.t('administration')
          click_link I18n.t('counterparties_model')
        end

        it { page.status_code.should eq 200 }
        it { current_path.should be_eql counterparties_path }

        context "then clicks link labeled 'View' next to a single existing counterparty record" do
          before { click_link I18n.t('view') }

          it { page.status_code.should eq 200 }
          it { current_path.should be_eql counterparty_path(counterparty) }

          it { page.body.should have_content(I18n.t('counterparties.company_name')) }
          it { page.body.should have_content(I18n.t('counterparties.contact_name')) }
          it { page.body.should have_content(I18n.t('counterparties.contact_email')) }
          it { page.body.should have_content(I18n.t('counterparties.contact_phone')) }
          it { page.body.should have_content(I18n.t('counterparties.contact_address')) }

          it { page.body.should have_selector('a', text: I18n.t('counterparties.edit')) }
          it { page.body.should have_selector('a', text: I18n.t('counterparties.delete')) }

          context "after that, user clicks link labeled 'Edit Counterparty', is taken to 'Edit Counterparty' page" do
            before { click_link I18n.t('counterparties.edit') }

            it { page.status_code.should eq 200 }
            it { current_path.should be_eql edit_counterparty_path(counterparty) }

            context 'and fill the form with valid data' do
              before do
                fill_in(I18n.t('counterparties.company_name'), :with => 'Changed Company for test')
                fill_in(I18n.t('counterparties.contact_name'), :with => 'John Watson')
                fill_in(I18n.t('counterparties.contact_email'), :with => 'valid@email.zyx')
                fill_in(I18n.t('counterparties.contact_phone'), :with => '(555) 321-0987')
                click_button(I18n.t('save'))
              end

              it { current_path.should be_eql counterparty_path(counterparty) }
              it { page.should have_selector(".alert-success", text: "Changed Company for test was successfully updated.") }
              it { page.should have_selector("td", text: "Changed Company") }
            end

            context 'and fill the form with invalid data' do
              before do
                fill_in(I18n.t('counterparties.company_name'), :with => 'Changed Company for test')
                fill_in(I18n.t('counterparties.contact_name'), :with => '')
                fill_in(I18n.t('counterparties.contact_email'), :with => 'valid@email.zyx')
                fill_in(I18n.t('counterparties.contact_phone'), :with => '(555) 321-0987')
                click_button(I18n.t('save'))
              end

              it { current_path.should be_eql counterparty_path(counterparty) }
              it { page.should have_selector(".alert-danger", text: "#{I18n.t('counterparties.form_error_message')}") }
              it { page.should have_selector(".alert-danger ul li") }

              context 'then correct the mistake' do
                before do
                  fill_in(I18n.t('counterparties.contact_name'), :with => 'Contact name')
                  click_button(I18n.t('save'))
                end

                it { current_path.should be_eql counterparty_path(counterparty) }
                it { page.should have_selector(".alert-success", text: "Changed Company for test was successfully updated.") }
                it { page.should have_selector("td", text: "Contact name") }
              end

              context 'then cancels counterparty creation' do
                before do
                  click_link(I18n.t('cancel'))
                end

                it { current_path.should be_eql counterparties_path }
                it { page.should_not have_selector('.alert') }
              end
            end
          end
        end
      end
    end
  end

  describe "deletes a single counterparty record when viewing a single counterparty record: " do
    context 'User navigates to Dashboard' do
      before { visit root_path }

      it { page.status_code.should eq 200 }
      it { page.body.should have_selector('li.active', text: I18n.t('dashboard')) }

      context "then user hovers over the 'Administration' link, clicks 'Counterparties' link" do
        let!(:another_counterparty) { create :counterparty, company_name: 'Company for delete' }

        before do
          click_link I18n.t('administration')
          click_link I18n.t('counterparties_model')
        end

        context "then clicks link labeled 'View' next to a single existing counterparty record" do
          before { first(:link, I18n.t('view')).click }

          it { page.status_code.should eq 200 }
          it { current_path.should be_eql counterparty_path(another_counterparty) }

          it { page.body.should have_selector('a', text: I18n.t('counterparties.edit')) }
          it { page.body.should have_selector('a', text: I18n.t('counterparties.delete')) }
          it { page.should have_selector('td', text: 'Company for delete') }

          context "after that, user clicks link labeled 'Delete Counterparty', is taken to 'Edit Counterparty' page", js: true do
            before do
              click_link I18n.t('counterparties.delete')
            end

            it { current_path.should be_eql counterparties_path }
            it { page.should_not have_selector('td', text: 'Company for delete') }
            it { page.should have_selector(".alert-success", text: 'Company for delete was successfully deleted.') }
          end
        end
      end
    end
  end


  describe "deletes multiple counterparty records from the list of existing counterparty records: " do
    context 'User navigates to Dashboard' do
      before { visit root_path }

      it { page.status_code.should eq 200 }
      it { page.body.should have_selector('li.active', text: I18n.t('dashboard')) }

      context "then user hovers over the 'Administration' link, clicks 'Counterparties' link" do
        let!(:counterparty_one) { create :counterparty, contact_name: 'Counterparty one' }
        let!(:counterparty_two) { create :counterparty, contact_name: 'Counterparty two' }

        before do
          click_link I18n.t('administration')
          click_link I18n.t('counterparties_model')
        end

        it { page.should have_selector('td', text: 'Counterparty one') }
        it { page.should have_selector('td', text: 'Counterparty two') }

        context "then checks check box next to one or more records that they would like to delete", js: true do
          before do
            find(:css, "#counterparties_[value='#{counterparty_one.id}']").set(true)
            find(:css, "#counterparties_[value='#{counterparty_two.id}']").set(true)
            click_button(I18n.t('counterparties.delete_selected'))
          end

          it { current_path.should be_eql counterparties_path }
          it { page.should_not have_selector('td', text: 'Counterparty one') }
          it { page.should_not have_selector('td', text: 'Counterparty two') }
          it { page.should have_selector(".alert-success", text: 'Counterparties with id 1,2 was successfully deleted.') }
        end
      end
    end
  end
end
