FactoryGirl.define do
  factory :acp_schedule do
    sequence(:name) { |n| "Acp Schedule ##{n}" }
    description 'Acp schedule description'
    state
  end
end
