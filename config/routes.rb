SrecTrading::Application.routes.draw do
  devise_for :users

  resource :dashboard, only: :index

  resources :brokers do
    delete 'destroy_multiple', on: :collection
  end

  resources :counterparties do
    delete 'destroy_multiple', on: :collection
  end

  resources :price_buckets do
    delete 'destroy_multiple', on: :collection
  end

  resources :payment_method_overrides do
    delete 'destroy_multiple', on: :collection
  end

  root :to => 'dashboard#index'
end
