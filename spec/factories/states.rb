FactoryGirl.define do
  factory :state do
    sequence(:name) { |n| "State ##{n}" }
    abbreviation 'active'
    is_system_state false
    is_sale_state false
  end
end
