(1..10).each do |n|
  Broker.create!({
   company_name: "##{n} Company name",
   contact_name: "##{n} Broker name",
   contact_email: "seed#{n}@mail.com",
   contact_address: "123 Test St, Ste 300, Washington, DC 01234",
   contact_phone: "(555) 123-0987"
   })
end

(1..10).each do |n|
  Counterparty.create!({
   company_name: "##{n} Company name",
   contact_name: "##{n} Counterparty name",
   contact_email: "counterparty#{n}@mail.com",
   contact_address: "444 Test Str, 300, Liverpool, DC 01234",
   contact_phone: "(333) 123-0987"
   })
end

State.create!([{name: 'Open', abbreviation: 'open'}, {name: 'Pending', abbreviation: 'pending'}, {name: 'Closed', abbreviation: 'closed'}])

(1..10).each do |n|
  ClearingPriceBucket.create!({
   name: "Clearing Price Bucket ##{n} ",
   description: "Bucket description"
   })
end
