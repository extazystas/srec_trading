class ClearingPriceBucket < ActiveRecord::Base
  attr_accessible :name, :description, :applicable_state_id

  validates :name, presence: true

  belongs_to :applicable_state, class_name: 'State'
end
