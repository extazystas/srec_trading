class PaymentMethodOverridesController < ApplicationController
  add_breadcrumb I18n.t('dashboard'), :root_path
  add_breadcrumb I18n.t('payment_method_overrides_model'), :payment_method_overrides_path

  def index
    @payment_method_overrides = PaymentMethodOverride.order('created_at DESC')
  end

  def new
    @payment_method_override = PaymentMethodOverride.new

    add_breadcrumb I18n.t('payment_method_overrides.new')
  end

  def show
    @payment_method_override = PaymentMethodOverride.find(params[:id])

    add_breadcrumb @payment_method_override.name
  end

  def edit
    @payment_method_override = PaymentMethodOverride.find(params[:id])

    add_breadcrumb @payment_method_override.name, payment_method_override_path(@payment_method_override)
    add_breadcrumb I18n.t('edit')
  end

  def create
    @payment_method_override = PaymentMethodOverride.new(params[:payment_method_override])

    if @payment_method_override.save
      redirect_to payment_method_overrides_path, notice: "#{@payment_method_override.name} was successfully created."
    else
      render :new
    end
  end

  def update
    @payment_method_override = PaymentMethodOverride.find(params[:id])

    if @payment_method_override.update_attributes(params[:payment_method_override])
      redirect_to payment_method_override_path(@payment_method_override), notice: "#{@payment_method_override.name} was successfully updated."
    else
      render :edit
    end
  end

  def destroy
    @payment_method_override = PaymentMethodOverride.find(params[:id]).destroy
    redirect_to payment_method_overrides_path, notice: "#{@payment_method_override.name} was successfully deleted."
  end

  def destroy_multiple
    if params[:payment_method_overrides].present?
      PaymentMethodOverride.destroy(params[:payment_method_overrides])
      redirect_to payment_method_overrides_path, notice: "Payment method overrides with id #{params[:payment_method_overrides].join(',')} was successfully deleted."
    else
      redirect_to payment_method_overrides_path, alert: 'No Payment method override was selected'
    end
  end
end
