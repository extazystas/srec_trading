class CreateAcpSchedules < ActiveRecord::Migration
  def change
    create_table :acp_schedules do |t|
      t.string :name
      t.text :description
      t.integer :state_id

      t.timestamps
    end
  end
end
