require 'spec_helper'

describe AcpSchedule do
  let(:acp_schedule) { create :acp_schedule }

  it { should validate_presence_of(:name) }

  it { should belong_to(:state) }
end
