class BrokersController < ApplicationController
  add_breadcrumb I18n.t('dashboard'), :root_path
  add_breadcrumb I18n.t('brokers_model'), :brokers_path

  def index
    @brokers = Broker.order('created_at DESC')
  end

  def new
    @broker = Broker.new

    add_breadcrumb I18n.t('brokers.new')
  end

  def show
    @broker = Broker.find(params[:id])

    add_breadcrumb @broker.company_name
  end

  def edit
    @broker = Broker.find(params[:id])

    add_breadcrumb @broker.company_name, broker_path(@broker)
    add_breadcrumb I18n.t('edit')
  end

  def create
    @broker = Broker.new(params[:broker])

    if @broker.save
      redirect_to brokers_path, notice: "#{@broker.company_name} was successfully created."
    else
      render :new
    end
  end

  def update
    @broker = Broker.find(params[:id])

    if @broker.update_attributes(params[:broker])
      redirect_to broker_path(@broker), notice: "#{@broker.company_name} was successfully updated."
    else
      render :edit
    end
  end

  def destroy
    @broker = Broker.find(params[:id]).destroy
    redirect_to brokers_path, notice: "#{@broker.company_name} was successfully deleted."
  end

  def destroy_multiple
    if params[:brokers].present?
      Broker.destroy(params[:brokers])
      redirect_to brokers_path, notice: "Brokers with id #{params[:brokers].join(',')} was successfully deleted."
    else
      redirect_to brokers_path, alert: 'No brokers was selected'
    end
  end
end
