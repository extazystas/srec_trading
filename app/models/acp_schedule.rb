class AcpSchedule < ActiveRecord::Base
  attr_accessible :name, :description

  validates :name, presence: true

  belongs_to :state
  has_many :payment_method_overrides, foreign_key: :per_acp_acp_schedule_id
end
