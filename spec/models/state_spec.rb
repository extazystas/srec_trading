require 'spec_helper'

describe State do
  let(:state) { create :state }

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:abbreviation) }

  it { should allow_value(true).for(:is_system_state) }
  it { should allow_value(false).for(:is_system_state) }
  it { should_not allow_value(nil).for(:is_system_state) }
  it { should allow_value(true).for(:is_sale_state) }
  it { should allow_value(false).for(:is_sale_state) }
  it { should_not allow_value(nil).for(:is_sale_state) }

  it { should have_many(:acp_schedules) }
  it { should have_many(:clearing_price_buckets) }
end
