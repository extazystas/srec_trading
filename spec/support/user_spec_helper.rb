module UserSpecHelper
  def sign_in_with(user)
    visit new_user_session_path
    fill_in('user_email', :with => user.email)
    fill_in('user_password', :with => user.password)
    click_button I18n.t('sign_in')
  end

  def user_should_be_signed_in
    page.should have_content(I18n.t("administration"))
  end

  def user_should_be_signed_out
    page.body.should_not have_content I18n.t("administration")
  end
end
