class CreateStates < ActiveRecord::Migration
  def change
    create_table :states do |t|
      t.string :name
      t.string :abbreviation
      t.boolean :is_system_state, default: false
      t.boolean :is_sale_state, default: false

      t.timestamps
    end
  end
end
