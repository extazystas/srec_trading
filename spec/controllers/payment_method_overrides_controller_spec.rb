require 'spec_helper'

describe PaymentMethodOverridesController do
  login_user

  let!(:payment_method_override) { create :payment_method_override }

  describe '#index' do
    before { get :index }

    it { should respond_with(:success) }
    it { should render_template(:index) }
    it { should_not set_the_flash }
  end

  describe '#show' do
    before { get :show, { id: payment_method_override.id } }

    it { should respond_with(:success) }
    it { should render_template(:show) }
    it { should_not set_the_flash }
  end

  describe '#new' do
    before { get :new }

    it { should respond_with(:success) }
    it { should render_template(:new) }
    it { should_not set_the_flash }
  end

  describe '#edit' do
    before { get :edit, { id: payment_method_override.id } }

    it { should respond_with(:success) }
    it { should render_template(:edit) }
    it { should_not set_the_flash }
  end

  describe '#create' do
    let(:params) { { payment_method_override: { name: 'Payment Method Override name', payment_method: 'fixed', payment_frequency: 'annually' } } }

    describe "with valid params" do
      before { post :create, params }

      it { should respond_with(:redirect) }
      it { should set_the_flash }
    end

    describe "with invalid params" do
      before do
        params[:payment_method_override][:payment_frequency] = nil
        post :create, params
      end

      it { should render_template(:new) }
      it { should_not set_the_flash }
    end
  end

  describe '#update' do
    let(:params) { { payment_method_override: { name: 'Changed Payment Method Override name', payment_method: 'fixed', payment_frequency: 'annually' }, id: payment_method_override.id } }

    describe "with valid params" do
      before { post :update, params }

      it { should respond_with(:redirect) }
      it { should set_the_flash }
    end

    describe "with invalid params" do
      before do
        params[:payment_method_override][:payment_method] = nil
        post :create, params
      end

      it { should render_template(:new) }
      it { should_not set_the_flash }
    end
  end

  describe '#destroy' do
    before do
      @payment_method_overrides_count = PaymentMethodOverride.count
      post :destroy, { id: payment_method_override.id }
    end

    it { PaymentMethodOverride.count.should be_eql(@payment_method_overrides_count - 1) }
    it { should redirect_to(payment_method_overrides_path) }
    it { should set_the_flash[:notice] }
  end

  describe '#destroy_multiple' do
    context 'with selected payment_method_overrides' do
      let!(:payment_method_override_one) { create :payment_method_override }
      let!(:payment_method_override_two) { create :payment_method_override }

      before do
        @payment_method_overrides_count = PaymentMethodOverride.count
        post :destroy_multiple, { payment_method_overrides: [payment_method_override_one.id, payment_method_override_two.id] }
      end

      it { PaymentMethodOverride.count.should be_eql(@payment_method_overrides_count - 2) }
      it { should redirect_to(payment_method_overrides_path) }
      it { should set_the_flash[:notice] }
    end

     context 'without selected payment_method_overrides' do
      before do
        @payment_method_overrides_count = PaymentMethodOverride.count
        post :destroy_multiple
      end

      it { PaymentMethodOverride.count.should be_eql(@payment_method_overrides_count) }
      it { should redirect_to(payment_method_overrides_path) }
      it { should set_the_flash[:alert] }
    end
  end
end
