class CreateClearingPriceBuckets < ActiveRecord::Migration
  def change
    create_table :clearing_price_buckets do |t|
      t.string :name
      t.text :description
      t.integer :applicable_state_id

      t.timestamps
    end
  end
end
